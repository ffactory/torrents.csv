# Rescan everything that hasn't been scanned in a while
cd ../
awk -F';' '$8 < $(date -d "6 months ago" "+%s")' torrents.csv | cut -d ';' -f1 > hashes_to_rescan
grep -vFf hashes_to_rescan infohashes_scanned.txt > new_infohashes_scanned
rm hashes_to_rescan
mv new_infohashes_scanned infohashes_scanned.txt

