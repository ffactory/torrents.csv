# This refetches the seeder counts for everthing in torrents.csv, and updates the seeder counts
echo "Refetching seeder counts ..."
cd ..
torrents_csv="`pwd`/torrents.csv"
torrents_removed="`pwd`/torrents_removed.csv"
prune_currents_tmps="`pwd`/prune_currents_tmps"
mkdir $prune_currents_tmps
cd $prune_currents_tmps

cp $torrents_csv tmp

# Extract the header
header=$(head -n1 tmp) 
sed -i '1d' tmp

cat tmp | cut -d ';' -f1 > tmp2
mv tmp2 tmp

# Split these up into 2000 file batches
split -l 2000 tmp tmp_

> no_seeds
for f in tmp_*; do
  echo "Fetching seeds..."
  echo $f
  torrent-tracker-health --torrent "$f" > health

  # Select the infohashes with zero seeders
  # append to a no seeds file
  jq '.results[] | select(.seeders==0) | .hash' health | tr -d \" >> no_seeds
  rm $f
done

# Remove those lines from the file
grep -vwF -f no_seeds $torrents_csv > $torrents_removed

cd ..
rm $prune_currents_tmps
rm health














