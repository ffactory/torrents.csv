FROM node:10-jessie as node
#If encounter Invalid cross-device error -run on host 'echo N | sudo tee /sys/module/overlay/parameters/metacopy'
COPY server/ui /app/server/ui
WORKDIR /app/server/ui
RUN yarn
RUN yarn build

FROM rust:1.33 as rust
WORKDIR /app/server
# cache the build
RUN USER=root cargo new service
WORKDIR /app/server/service
# copy over your manifests
COPY server/service/Cargo.toml server/service/Cargo.lock ./
RUN cargo build --release
RUN rm src/*.rs
COPY server/service/src ./src
RUN rm -r ./target/release/.fingerprint/torrents-csv-*
RUN cargo build --release

FROM debian:jessie-slim as volume
COPY torrents.csv  /db/
COPY torrent_files.json /db/

FROM debian:jessie-slim
RUN apt update && apt install -y sqlite3 curl
RUN curl -LO https://github.com/BurntSushi/ripgrep/releases/download/0.10.0/ripgrep_0.10.0_amd64.deb
RUN dpkg -i ripgrep_0.10.0_amd64.deb
RUN curl -LO https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
RUN chmod +x jq-linux64
RUN mv jq-linux64 /usr/bin/jq

COPY --from=node /app/server/ui/dist /app/dist
COPY --from=rust /app/server/service/target/release/torrents-csv-service /app/
COPY --from=volume /db/torrents.csv /app/
COPY --from=volume /db/torrent_files.json /app/
COPY scripts /app/scripts
WORKDIR /app/scripts/
RUN ["/bin/bash", "./build_sqlite.sh"]
EXPOSE 8080
WORKDIR /app/
